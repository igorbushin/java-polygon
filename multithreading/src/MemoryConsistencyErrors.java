class MemoryConsistencyErrors {
    class Counter { int c = 0; }
    void demo(){
        Counter counter = new Counter();
        Thread incrementer = new Thread() {
            public void run() { for (int i = 0; i < 1e6; ++i) counter.c ++;}
        };
        incrementer.start();
        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(counter.c);
    }
}
