class Deadlock {
    class Friend {
        final String name;
        Friend(String name) { this.name = name;}
        synchronized void bow(Friend bower) {
            System.out.println(this.name + " " + bower.name + " has bowed to me!");
            bower.bowBack(this);
        }
        synchronized void bowBack(Friend bower) {
            System.out.println(this.name + " " + bower.name + " has bowed back to me!");
        }
    }

    void demo() {
        final Friend alphonse = new Friend("Alphonse");
        final Friend gaston = new Friend("Gaston");
        new Thread(new Runnable() {public void run() { alphonse.bow(gaston); }}).start();
        new Thread(new Runnable() {public void run() { gaston.bow(alphonse); }}).start();
    }
}