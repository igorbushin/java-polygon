public class Multithreading {
    private static void threadMessage(String message) {
        String threadName = Thread.currentThread().getName();
        System.out.println(threadName + " : " + message);
    }
    private class SubThread extends Thread {}
    private class BackgroundThread implements Runnable {
        public void run() {
            Thread.currentThread().setName("back");
            try {
                for (int i = 0; i < 10; i++) {
                    Thread.sleep(1000);
                    threadMessage("Current step: " + Integer.toString(i));
                }
            } catch (InterruptedException e) {
                threadMessage("I wasn't done!");
            }
        }
    }

    public void demo() throws InterruptedException {
        long patience = 4000;
        long startTime = System.currentTimeMillis();

        threadMessage("Starting background thread");
        Thread thread = new Thread(new BackgroundThread());
        thread.start();

        threadMessage("Waiting for background thread to finish");
        while (thread.isAlive()) {
            threadMessage("Still waiting... thread state: " + thread.getState().name());
            thread.join(1000);
            if (((System.currentTimeMillis() - startTime) > patience) && thread.isAlive()) {
                threadMessage("Tired of waiting!");
                thread.interrupt();
                thread.join();
            }
        }
        threadMessage("Finally!");
    }
}