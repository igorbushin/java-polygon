public class Main {
    public static void main(String args[]) {
        //new Multithreading().demo();
        //new Deadlock().demo();
        //new ThreadInterference().demo();
        //new MemoryConsistencyErrors().demo();
        //new DropPattern().demo();
        new BackgroundTaskDemo().demo();
    }
}