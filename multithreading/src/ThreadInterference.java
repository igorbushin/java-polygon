class ThreadInterference {
    class Counter { int c = 0; }
    void demo(){
        Counter counter = new Counter();
        Thread incrementer = new Thread(() -> { for (int i = 0; i < 1e6; ++i) counter.c++;});
        Thread decrementer = new Thread(()-> { for (int i = 0; i < 1e6; ++i) counter.c--;});
        incrementer.start();
        decrementer.start();
        try {
            incrementer.join();
            decrementer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(counter.c);
    }
}
