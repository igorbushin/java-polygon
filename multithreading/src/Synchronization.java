public class Synchronization {
    public volatile long aLong;
    public volatile double aDouble; //other primitive types & refs already volatile
    public synchronized void syncMethod() { }
    public void innerSyncMethod() {
        synchronized (this) { }
    }
}
