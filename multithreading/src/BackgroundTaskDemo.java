import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

class BackgroundTaskDemo {

    abstract class BackgroundTask{
        Thread thread;
        ObjectProperty<Boolean> isRun;

        private boolean isRun(){
            return thread != null && thread.isAlive();
        }

        ObjectProperty<Boolean> run(){
            if(isRun()) {
                kill();
            }
            isRun =  new SimpleObjectProperty<>(false);
            thread = new Thread() {
                @Override
                public void run() {
                    try {
                        isRun.setValue(true);
                        taskBody();
                        isRun.setValue(false);
                    } catch (InterruptedException e) {
                        isRun.setValue(false);
                    }
                }
            };
            thread.start();
            return isRun;
        }

        void kill(){
            if(isRun()) thread.interrupt();
        }

        abstract void taskBody() throws InterruptedException;
    }

    class MyTask extends BackgroundTask {

        @Override
        public void taskBody() {
            try {
                Thread.sleep(3000);
                System.out.println("task done, thread int?: " + Thread.interrupted());
            }
            catch (InterruptedException e){
                System.out.println("die");
            }
        }
    }
    void demo(){
        MyTask myTask = new MyTask();
        ObjectProperty isRun = myTask.run();
        isRun.addListener(observable -> {
            System.out.println("is run: " + isRun.get());
        });
        myTask.kill();
    }
}
