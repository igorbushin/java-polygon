import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class BadJavaInterview {

    public static void main(String[] args) {
        //#2: какое сравнение неверно
        System.out.print("#2: ");
        String string = "hello";
        StringBuilder builder = new StringBuilder("hello");
        StringBuilder builder2 = new StringBuilder("hello");
        //System.out.println(string == builder); <= CE
        System.out.print(string.equals(builder));//false
        System.out.println(builder.equals(builder2));//false

        //#3 какой метод вызовется?
        System.out.print("#3: ");
        byte var = 1;
        System.out.println(action(var,var));//2

        //#4
        System.out.print("#4: ");
        http://www.site.domen
        System.out.println("it's possible");

        //#5 какие сравнения true
        System.out.print("#5: ");
        String s1 = "hello";
        final String s2 = "hel";
        String s3 = "lo";
        String s4 = "hello";
        System.out.print(s1 == s4);//true
        System.out.print(s1 == s2 + s3);//false runtime concat because s2 final
        System.out.println(s1 == s2 + "lo");//true

        //#6 used valueOf(int i) method and return cache for -127 <= i <= 127
        // {if i >= IntegerCache.low && IntegerCache.high)
        //      return IntegerCache.cache(i + (-IntegerCash.low));
        //  return Integer(i);}
        System.out.print("#6: ");
        Long a = 111L;
        Long b = 111L;
        Long c = 222L;
        Long d = 222L;
        System.out.print(a == b);//true
        System.out.println(c == d);//false

        //#7 whats happened?
        //work();

        //#8 whats happened?
        System.out.println("#8: ");
        int[] arr = new int[1];
        int i = 2;
        try { arr[--i] = 1 / --i; }
        catch (Exception e) { System.out.println(e);} //ArithmeticException
        int j = 2;
        try { arr[--j] = --j; }
        catch (Exception e) { System.out.println(e);} //IndexOutOfBounds

        //#9 Thread end.Main end
        System.out.print("#9: ");
        Thread thread = new Thread(() -> System.out.print("Thread end."));
        synchronized (thread) {
            thread.start();
            try {
                thread.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Main end");

        //#10
        System.out.print("#10: ");
        try {
            Set<Number> set = new TreeSet<Number>();
            set.add(1);
            set.add(1L);
            set.add(1.0);
            System.out.println(set.size());
        }
        catch (Exception e) {
            System.out.println(e); //cast exception long to int
        }

        //#11
        System.out.print("#11: ");
        List longs = new ArrayList<Long>();
        longs.add(1L);
        longs.add(1.0);
        longs.add(new Object());
        longs.add("I am LONG!!");
        System.out.println(longs.size()); // 4

        //#12
        System.out.print("#12: ");
        try {
            try {
                Test.run();
            } catch (Throwable e) {
                Test.run();
            }
        }
        //java.lang.NoClassDefFoundError: Could not initialize class BadJavaInterview$Test
        catch (Throwable e) {
            System.out.println(e);
        }

        //#14
         try {
             System.out.println("14: ");
             System.out.println(ValuesEnum.A);
             Class<? extends ValuesClass> valuesClassClass = ValuesClass.A.getClass();
             System.out.println(valuesClassClass.getDeclaredField("A").getName());
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    enum ValuesEnum {
        A(1),B(2),C(3);
        ValuesEnum(int i) {System.out.print(i);}
        static {System.out.print("static");}
    }
    static class ValuesClass {
        static ValuesClass A = new ValuesClass(1);
        static ValuesClass B = new ValuesClass(2);
        static ValuesClass C = new ValuesClass(3);
        private int i;
        ValuesClass(int i) {System.out.print(this.i = i);}
        static {System.out.print("static");}

        @Override
        public String toString() {
            return getClass().getName();
        }
    }

    static int action(byte... args){return 1;}//lowest priority
    static int action(long l1, short s2){return 2;}//primitive types have priority
    static int action(Byte i1, Byte i2){return 3;}

    private static void work() {
        try {
            work();
        } finally {
            work(); //start again recursion after StackOverflow
        }
    }

    static class Test {
        static {
            if(true) throw  new NullPointerException();
        }
        static void run(){}
    }

    interface I1 {
        void copy() throws IOException, IllegalArgumentException;
    }

    interface I2 {
        void copy() throws FileNotFoundException, InterruptedException;
    }

    public class A implements I1, I2 {
        @Override //this two exceptions or one or no one;
        public void copy() throws IllegalArgumentException, FileNotFoundException {

        }
    }
}
